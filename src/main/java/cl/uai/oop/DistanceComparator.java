/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;
import java.util.*;

/**
 *
 * @author andre
 */
public class DistanceComparator implements Comparator<Place>{
    private final PlaceLocation location;
    public DistanceComparator(PlaceLocation location) {
        this.location = location;
    }

    @Override
    public int compare(Place t, Place t1) {
        if(Utils.distance(location, t.getUbicacion()) < Utils.distance(location, t1.getUbicacion())){
            return -1;
        } else if(Utils.distance(location, t.getUbicacion()) > Utils.distance(location, t1.getUbicacion())){
            return 1;
        } else{
            return 0;
        }
    }
    
}
