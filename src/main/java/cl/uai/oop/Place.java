/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import java.util.Date;
/**
 *
 * @author andre
 */
public class Place implements Comparable<Place>{
    private final String nombre;
    private final String descripcion;
    private final PlaceEnum lugar;
    private final PlaceLocation ubicacion;
    private Date horaApertura;
    private Date horaCierre;

    public Place(String nombre, String descripcion, PlaceEnum lugar, PlaceLocation ubicacion, Date horaApertura, Date horaCierre) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.lugar = lugar;
        this.ubicacion = ubicacion;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Enum getLugar() {
        return lugar;
    }

    public PlaceLocation getUbicacion() {
        return ubicacion;
    }

    public Date getHoraApertura() {
        return horaApertura;
    }

    public Date getHoraCierre() {
        return horaCierre;
    }

    public void setHoraApertura(Date horaApertura) {
        this.horaApertura = horaApertura;
    }

    public void setHoraCierre(Date horaCierre) {
        this.horaCierre = horaCierre;
    }
    
    public boolean equals(Object obj){
        if(obj instanceof String){
            return obj.equals(this.nombre);
        }else if(obj instanceof Place){
            Place copia = (Place) obj;
            return copia.nombre.equals(this.nombre);
        }
        return false;
    }

    @Override
    public int compareTo(Place t) {
        return this.nombre.compareTo(t.getNombre());
    }
    
    
    
}
