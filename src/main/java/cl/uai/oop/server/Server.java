/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.server;

import cl.uai.oop.DistanceComparator;
import cl.uai.oop.PlaceLocation;
import cl.uai.oop.Place;
import cl.uai.oop.Utils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.*;
import java.util.ArrayList;
import java.io.IOException;

/**
 *
 * @author andre
 */
public class Server extends Thread{
    ServerSocket serverSocket;
    Socket clientSocket;
    ArrayList<Place> lista = new ArrayList<Place>();

    public Server(ServerSocket serverSocket, Socket clientSocket, ArrayList lista) {
        this.serverSocket = serverSocket;
        this.clientSocket = clientSocket;
        this.lista = lista;
    }
    
    public void run(){
        try{
            clientSocket = serverSocket.accept();
            InputStream is = clientSocket.getInputStream();
            OutputStream os = clientSocket.getOutputStream();
            DataInputStream in = new DataInputStream(is);
            DataOutputStream out = new DataOutputStream(os);
            String command = in.readUTF();
            String[] parameters = command.split(";");
            int option = Integer.parseInt(parameters[0]);
            if (option == 1) {
                PlaceLocation pl = new PlaceLocation(Double.parseDouble(parameters[1]), Double.parseDouble(parameters[2]));
                //TODO: get the nearest places sorted by distance
                Collections.sort(lista,new DistanceComparator(pl));
                //TODO: return the list of place to client socket
                out.write(lista.size());
                for (int i = 0; i < lista.size(); i++){
                    double dist = Utils.distance(pl, lista.get(i).getUbicacion());
                    out.writeUTF(lista.get(i).getNombre() + " a " + dist + " kilometros.");
                }
            } else if (option == 2) {
                String placeName = parameters[1];
                //TODO: get the place using the name
                int index = lista.indexOf(placeName);
                //TODO: return the place to client socket
                if(index != -1){
                    String descripcion = lista.get(index).getDescripcion();
                    out.writeUTF(descripcion);
                    } else {
                        out.writeUTF("No se encontro un lugar con ese nombre");
                    }
            }
        } catch (IOException ex) {
        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
