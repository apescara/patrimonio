/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import cl.uai.oop.DistanceComparator;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author gohucan
 */
public class UtilsTest {
    
    private Faker faker = new Faker();
    private List<PlaceLocation> placeLocation = new ArrayList<>(100);
    private List<Place> place = new ArrayList<>(100);
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        for(int i = 0; i< 100; i++){
            double latitud = Double.parseDouble(faker.address().latitude().replace(',','.'));
            double longitud = Double.parseDouble(faker.address().longitude().replace(',','.'));
            placeLocation.add(new PlaceLocation(latitud,longitud));
        }
        for(int i = 0; i<100; i++){
            place.add(new Place(faker.name().name(), faker.lorem().sentence(), 
                    PlaceEnum.ESTATUA, placeLocation.get(i), faker.date().past(i, TimeUnit.DAYS), 
                    faker.date().future(i, TimeUnit.DAYS)));
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of distance method, of class Utils.
     */
    @Test
    public void testDistance() {
        Collections.sort(place, new DistanceComparator(place.get(0).getUbicacion()));
        double distancia;
        for (int i = 0; i<99; i++){
            assert Utils.distance(place.get(0).getUbicacion(), place.get(i).getUbicacion()) <= 
                    Utils.distance(place.get(0).getUbicacion(), place.get(i + 1).getUbicacion());
        }
        for (int i = 0; i < 100; i++){
                distancia = Utils.distance(place.get(0).getUbicacion(), place.get(i).getUbicacion());
                System.out.println("La distancia del lugar 0 al lugar " + i + " es de " + distancia + " kilometros");
        }
        
    }
    
}
